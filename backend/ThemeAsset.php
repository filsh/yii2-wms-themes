<?php

namespace wms\themes\backend;

use yii\web\AssetBundle;

/**
 * Theme main asset bundle.
 */
class ThemeAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@vendor/almasaeed2010/adminlte/dist';

    /**
     * @inheritdoc
     */
    public $css = [
        'css/AdminLTE.min.css',
        'css/skins/_all-skins.min.css',
    ];

    public $js = [
        'js/app.min.js'
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        \yii\web\YiiAsset::class,
        \yii\web\JqueryAsset::class,
        \yii\bootstrap\BootstrapAsset::class,
        \yii\bootstrap\BootstrapPluginAsset::class,
        \wms\themes\backend\FontAsset::class,
    ];
}
