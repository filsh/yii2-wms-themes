<?php

namespace wms\themes\backend;

use Yii;
use yii\i18n\PhpMessageSource;

class Theme extends \yii\base\Theme
{
    /**
     * @inheritdoc
     */
    public $pathMap = [
        '@backend/views' => '@wms/themes/backend/views'
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->registerTranslations();
        
        Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = [
            'sourcePath' => '@vendor/almasaeed2010/adminlte/bootstrap',
            'css' => [
                'css/bootstrap.min.css'
            ]
        ];
        Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapPluginAsset'] = [
            'sourcePath' => '@vendor/almasaeed2010/adminlte/bootstrap',
            'js' => [
                'js/bootstrap.min.js'
            ]
        ];
    }
    
    public function registerTranslations()
    {
        if(!isset(Yii::$app->i18n->translations['wms/themes/*'])) {
            Yii::$app->i18n->translations['wms/themes/*'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ . '/messages',
            ];
        }
    }
    
    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('wms/themes/' . $category, $message, $params, $language);
    }
}
