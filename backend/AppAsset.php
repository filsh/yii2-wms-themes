<?php

namespace wms\themes\backend;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@wms/themes/backend/assets';
    
    public $css = [
        'css/main.css',
    ];
    
    public $js = [
        'js/main.js'
    ];

    public $depends = [
        \wms\themes\backend\ThemeAsset::class
    ];
    
    public $publishOptions = [
        'forceCopy' => YII_DEBUG
    ];
}
