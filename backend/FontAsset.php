<?php

namespace wms\themes\backend;

use yii\web\AssetBundle;

/**
 * Theme fonts asset bundle.
 */
class FontAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $css = [
        'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css',
        'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
    ];
}
